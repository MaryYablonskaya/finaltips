//
//  FourthViewController.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 14.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "FourthViewController.h"
#import "PYCurrencyCounter.h"
#import "AppDelegate.h"
@implementation FourthViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.currencyHelper  = [[PYCurrencyCounter alloc]init];
}
- (IBAction)countTipsClick:(id)sender {
    self.counter.checkCount = [[self.checkOutlet text] doubleValue];
    AppDelegate * delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Restaurant" inManagedObjectContext:context];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entityDesc];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:fetchRequest error:&error];
    NSLog(@"%@",objects[0]);
    [self.tipsOutlet setText:[NSString stringWithFormat:@"%f",[self.counter receiveTipsCount] ]];
    double d = [self.currencyHelper dollarsCountWithRoubles: [self.counter receiveTipsCount]];
    
    [self.dollars setText:
     [NSString stringWithFormat:@"%f",d]
     ];
    d = [self.currencyHelper euroCountWithRoubles: [self.counter receiveTipsCount]];
    [self.euro setText:
     [NSString stringWithFormat:@"%f",d]
     ];
    
}
@end
