//
//  SecondViewController.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 12.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PYTipsHelper.h"
@interface SecondViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>
@property PYTipsHelper * counter;
@property NSString * option;
@property NSString *restaurant;
- (IBAction)onFoodButtonClick:(id)sender;
- (IBAction)onServiceButtonClick:(id)sender;
- (IBAction)onSpeedButtonClick:(id)sender;
- (IBAction)onAtmosphereButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIPickerView *listView;

- (IBAction)onOneClick:(id)sender;
- (IBAction)onTwoClick:(id)sender;
- (IBAction)onThreeClick:(id)sender;
- (IBAction)onFourClick:(id)sender;
- (IBAction)onSecondNextClick:(id)sender;

- (IBAction)onDiscardClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *foodButton;

@property (weak, nonatomic) IBOutlet UIButton *serviceButton;
@property (weak, nonatomic) IBOutlet UIButton *speedButton;
@property (weak, nonatomic) IBOutlet UIButton *atmosphereButton;

@property (weak, nonatomic) IBOutlet UIButton *oneButton;
@property (weak, nonatomic) IBOutlet UIButton *twoButton;
@property (weak, nonatomic) IBOutlet UIButton *threeButton;
@property (weak, nonatomic) IBOutlet UIButton *fourButton;

@property (strong) NSManagedObjectContext *managedObjectContext;
@property (strong) NSManagedObjectModel *managedObjectModel;
@property (strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
-(id)init;
@end
