//
//  Restaurant.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 18.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Restaurant : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSString * name;

@end
