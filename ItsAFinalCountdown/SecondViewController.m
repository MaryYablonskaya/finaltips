
//  SecondViewController.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 12.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "RestaurantViewController.h"
#import "AppDelegate.h"
@interface SecondViewController ()

@end

@implementation SecondViewController
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 10;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    /*AppDelegate * delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Restaurant" inManagedObjectContext:context];
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entityDesc];
            NSError *error;
            NSArray *objects = [context executeFetchRequest:fetchRequest error:&error];
    NSLog(@"%@",objects[0]);*/
    
    switch (row)
    {
        /*case 0: return @"Monday";
        case 1: return @"Tuesday";
        case 2: return @"Wednesday";
        case 3: return @"Thursday";
        case 4: return @"Friday";
        case 5: return @"Saturday";
        case 6: return @"Sunday";*/
        case 0: return @"New Restaurant";
        default: return nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view, typically from a nib.
    self.listView.delegate = self;
     self.listView.dataSource = self;
     self.listView.showsSelectionIndicator = YES;
    _counter = [[PYTipsHelper alloc] init];
    _option = @"New Restaurant";
    _restaurant = @"";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onFoodButtonClick:(id)sender {
    self.option = @"Food";
}

- (IBAction)onServiceButtonClick:(id)sender {
    self.option = @"Service";
}

- (IBAction)onSpeedButtonClick:(id)sender {
    self.option = @"Speed";
}

- (IBAction)onAtmosphereButtonClick:(id)sender {
    self.option = @"Atmo";
}

- (IBAction)onOneClick:(id)sender {
    if([self.option isEqualToString:@"Food"])
    {
        self.counter.foodPriority = 1;
        self.foodButton.enabled=NO;
        self.oneButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Service"])
    {
        self.counter.servicePriority = 1;
        self.serviceButton.enabled=NO;
        self.oneButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Speed"])
    {
        self.counter.speedPriority = 1;
        self.speedButton.enabled=NO;
        self.oneButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Atmo"])
    {
        self.counter.atmospherePriority = 1;
        self.atmosphereButton.enabled=NO;
        self.oneButton.enabled=NO;
        
    }
}

- (IBAction)onTwoClick:(id)sender {
    if([self.option isEqualToString:@"Food"])
    {
        self.counter.foodPriority = 2;
        self.foodButton.enabled=NO;
        self.twoButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Service"])
    {
        self.counter.servicePriority = 2;
        self.serviceButton.enabled=NO;
        self.twoButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Speed"])
    {
        self.counter.speedPriority = 2;
        self.speedButton.enabled=NO;
        self.twoButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Atmo"])
    {
        self.counter.atmospherePriority = 2;
        self.atmosphereButton.enabled=NO;
        self.twoButton.enabled=NO;
        
    }
}

- (IBAction)onThreeClick:(id)sender {
    if([self.option isEqualToString:@"Food"])
    {
        self.counter.foodPriority = 3;
        self.foodButton.enabled=NO;
        self.threeButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Service"])
    {
        self.counter.servicePriority = 3;
        self.serviceButton.enabled=NO;
        self.threeButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Speed"])
    {
        self.counter.speedPriority = 3;
        self.speedButton.enabled=NO;
        self.threeButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Atmo"])
    {
        self.counter.atmospherePriority = 3;
        self.atmosphereButton.enabled=NO;
        self.threeButton.enabled=NO;
        
    }
}

- (IBAction)onFourClick:(id)sender {
    if([self.option isEqualToString:@"Food"])
    {
        self.counter.foodPriority = 4;
        self.foodButton.enabled=NO;
        self.fourButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Service"])
    {
        self.counter.servicePriority = 4;
        self.serviceButton.enabled=NO;
        self.fourButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Speed"])
    {
        self.counter.speedPriority = 4;
        self.speedButton.enabled=NO;
        self.fourButton.enabled=NO;
        
    }
    if([self.option isEqualToString:@"Atmo"])
    {
        self.counter.atmospherePriority = 4;
        self.atmosphereButton.enabled=NO;
        self.fourButton.enabled=NO;
        
    }
}

- (IBAction)onSecondNextClick:(id)sender {
    
    self.restaurant = [self.listView.delegate pickerView:self.listView titleForRow:[self.listView  selectedRowInComponent:0] forComponent:0];
    
    
    if(![self.restaurant isEqualToString:@"New Restaurant"])
    { ThirdViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"Third"];
    view.counter = self.counter;
        //!!!view.restaurant
    [self presentModalViewController:view animated:YES];
    }
    else{
        RestaurantViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"Restaurant"];
        view.counter = self.counter;
        [self presentModalViewController:view animated:YES];
    }
    
}

- (IBAction)onDiscardClick:(id)sender {
    
        self.counter.foodPriority = -1;
        self.foodButton.enabled=YES;
        self.fourButton.enabled=YES;
    
        self.counter.servicePriority = -1;
        self.serviceButton.enabled=YES;
        self.oneButton.enabled=YES;
   
        self.counter.speedPriority = -1;
        self.speedButton.enabled=YES;
        self.twoButton.enabled=YES;
   
        self.counter.atmospherePriority = -1;
        self.atmosphereButton.enabled=YES;
        self.threeButton.enabled=YES;
    self.option=nil;
    self.restaurant = @"";
        
}
-(id)init{
    self=[super init];
    if(self)
    {
        _counter = [[PYTipsHelper alloc] init];
        _option = @"";
    }
    return self;
}
@end
