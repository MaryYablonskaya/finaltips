//
//  Restaurant.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 18.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "Restaurant.h"


@implementation Restaurant

@dynamic address;
@dynamic comments;
@dynamic name;

@end
