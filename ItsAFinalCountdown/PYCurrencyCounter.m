//
//  PYCurrencyCounter.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 16.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "PYCurrencyCounter.h"
#import "PYDataStore.h"
@implementation PYCurrencyCounter
-(id)init{
    self = [super init];
    if(self){
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.nbrb.by/Services/XmlExRates.aspx?ondate=10/30/2015"]];
    _del = [[PYDataStore alloc] init];
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:_del] ;
    }
    return self;
}
-(double)dollarsCountWithRoubles: (double)rubl{
    return rubl/[[self.del.dict valueForKey:@"Доллар США"] doubleValue];
}
-(double)euroCountWithRoubles: (double)rubl{
    return rubl/[[self.del.dict valueForKey:@"Евро"] doubleValue];
}
@end
