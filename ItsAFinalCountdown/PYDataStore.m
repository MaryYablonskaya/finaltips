//
//  PYDataStore.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 14.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "PYDataStore.h"
@implementation PYDataStore
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"Error %@", error);
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    //парсим
    NSXMLParser *rssParser = [[NSXMLParser alloc] initWithData:self.receivedData];
    rssParser.delegate = self;
    [rssParser parse];
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    self.receivedData = [[NSMutableData alloc] init];
    
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    self.currentElement = elementName;
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    self.dict  = [NSMutableDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithDouble:0] ,@"Доллар США",[NSNumber numberWithDouble:0],@"Евро",nil];
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([self.currentElement isEqualToString:@"Name"]) {
        if(![trimmedString isEqualToString:@""])
            self.currency = [NSString stringWithFormat:@"%@" ,trimmedString];
 
    } else if ([self.currentElement isEqualToString:@"Rate"]) {
        if(![trimmedString isEqualToString:@""]){
            if([self.currency isEqualToString:@"Доллар США"]){
                self.dict[@"Доллар США"] = [NSNumber numberWithDouble:[trimmedString doubleValue]];
            }
            if([self.currency isEqualToString:@"Евро"]){
                self.dict[@"Евро"] = [NSNumber numberWithDouble:[trimmedString doubleValue]];
            }
        }
    }
}@end
