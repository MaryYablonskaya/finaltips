//
//  RestaurantViewController.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 15.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "RestaurantViewController.h"
#import "Restaurant.h"
#import "ThirdViewController.h"
#import "AppDelegate.h"
@import CoreData;
@implementation RestaurantViewController

- (IBAction)OnSubmit:(id)sender {
    AppDelegate * delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    NSManagedObject *newObjectR;
    newObjectR = [NSEntityDescription insertNewObjectForEntityForName:@"Restaurant" inManagedObjectContext:context];
            
            [newObjectR setValue:[self.name text] forKey:@"name"];
            [newObjectR setValue:[self.address text] forKey:@"address"];
            [newObjectR setValue:[self.comments text] forKey:@"comments"];
    NSError *error;
    [context save:&error];
   /*NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant" inManagedObjectContext: ((AppDelegate*)([UIApplication sharedApplication].delegate)).managedObjectContext];
    
    Restaurant *restaurant = [[Restaurant alloc]initWithEntity:entity insertIntoManagedObjectContext: ((AppDelegate*)([UIApplication sharedApplication].delegate)).managedObjectContext];
    
    restaurant.address  = [self.address text];
    restaurant.name = [self.name text];
    restaurant.comments = [self.comments text];*/
    ThirdViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"Third"];
    view.counter = self.counter;
    //view.restaurant = restaurant;
    [self presentModalViewController:view animated:YES];
}
@end