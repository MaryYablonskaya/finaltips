//
//  FourthViewController.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 14.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PYTipsHelper.h"
#import "PYCurrencyCounter.h"
@interface FourthViewController : UIViewController
@property PYTipsHelper* counter;
@property PYCurrencyCounter * currencyHelper;
- (IBAction)countTipsClick:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *checkOutlet;
@property (weak, nonatomic) IBOutlet UITextField *tipsOutlet;
@property (weak, nonatomic) IBOutlet UITextField *dollars;
@property (weak, nonatomic) IBOutlet UITextField *euro;

@end
