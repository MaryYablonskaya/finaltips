//
//  ThirdViewController.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 14.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PYTipsHelper.h"
#import "Restaurant.h"
@interface ThirdViewController : UIViewController
- (IBAction)foodStepperClick:(UIStepper *)sender;
- (IBAction)serviceStepperClick:(UIStepper *)sender;
- (IBAction)speedStepperClick:(UIStepper *)sender;
- (IBAction)atmosphereStepperClick:(UIStepper *)sender;
@property (weak, nonatomic) IBOutlet UILabel *foodLab;
@property (weak, nonatomic) IBOutlet UILabel *serviceLab;
@property (weak, nonatomic) IBOutlet UILabel *speedLab;
@property (weak, nonatomic) IBOutlet UILabel *atmosphereLab;
@property Restaurant *restaurant;
@property PYTipsHelper* counter;
- (IBAction)onThirdNext:(id)sender;

@end
