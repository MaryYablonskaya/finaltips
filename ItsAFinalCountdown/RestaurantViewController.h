//
//  RestaurantViewController.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 15.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PYTipsHelper.h"
@interface RestaurantViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UITextField *comments;
- (IBAction)OnSubmit:(id)sender;
@property PYTipsHelper* counter;
@end
