//
//  RestaurantVisit.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 18.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "RestaurantVisit.h"


@implementation RestaurantVisit

@dynamic check;
@dynamic date;
@dynamic tips;
@dynamic restaurantName;

@end
