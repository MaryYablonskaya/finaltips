//
//  RestaurantVisit.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 18.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RestaurantVisit : NSManagedObject

@property (nonatomic, retain) NSNumber * check;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * tips;
@property (nonatomic, retain) NSString * restaurantName;

@end
