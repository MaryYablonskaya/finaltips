//
//  ThirdViewController.m
//  ItsAFinalCountdown
//
//  Created by fpmi on 14.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import "ThirdViewController.h"
#import "FourthViewController.h"
@implementation ThirdViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)foodStepperClick:(UIStepper *)sender {
   double val = [sender value];
    if(val>5||val<0)
    {
        return;
    }
    [self.foodLab setText: [NSString stringWithFormat:@"%d",(int)val]];
}

- (IBAction)serviceStepperClick:(UIStepper *)sender {
    double val = [sender value];
    if(val>5||val<0)
    {
        return;
    }
    [self.serviceLab setText: [NSString stringWithFormat:@"%d",(int)val]];
}

- (IBAction)speedStepperClick:(UIStepper *)sender {
    double val = [sender value];
    if(val>5||val<0)
    {
        return;
    }
    [self.speedLab setText: [NSString stringWithFormat:@"%d",(int)val]];
}

- (IBAction)atmosphereStepperClick:(UIStepper *)sender {
    double val = [sender value];
    if(val>5||val<0)
    {
        return;
    }
    [self.atmosphereLab setText: [NSString stringWithFormat:@"%d",(int)val]];
}
- (IBAction)onThirdNext:(id)sender {
    
    self.counter.foodQuality = [[self.foodLab text ]intValue];
    self.counter.serviceQuality = [[self.serviceLab text ]intValue];
    self.counter.speed = [[self.speedLab text ]intValue];
    self.counter.atmosphereQuality = [[self.atmosphereLab text ]intValue];
    FourthViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"Fourth"];
    view.counter = self.counter;
    [self presentModalViewController:view animated:YES];
}
@end
