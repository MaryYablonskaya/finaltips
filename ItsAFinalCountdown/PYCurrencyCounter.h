//
//  PYCurrencyCounter.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 16.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PYDataStore.h"
@interface PYCurrencyCounter : NSObject
@property NSURLConnection *connection;
@property PYDataStore * del;
-(id)init;
-(double)dollarsCountWithRoubles: (double)rubl;
-(double)euroCountWithRoubles: (double)rubl;
@end
