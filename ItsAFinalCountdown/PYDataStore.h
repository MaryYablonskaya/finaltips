//
//  PYDataStore.h
//  ItsAFinalCountdown
//
//  Created by fpmi on 14.12.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PYDataStore: NSObject <NSURLConnectionDelegate>
@property NSMutableData* receivedData;
@property NSString* currentElement;
@property NSString* currency;
@property NSMutableDictionary* dict;
@end
